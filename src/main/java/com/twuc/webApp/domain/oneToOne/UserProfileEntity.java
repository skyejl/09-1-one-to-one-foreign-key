package com.twuc.webApp.domain.oneToOne;

import javax.persistence.*;

// TODO
//
// 请创建 UserEntity 和 UserProfileEntity 之间的 one-to-one 关系。并且确保 UserProfileEntity
// 的数据表结构如下：
//
// user_profile_entity
// +─────────────────+──────────────+──────────────────────────────+
// | column          | type         | additional                   |
// +─────────────────+──────────────+──────────────────────────────+
// | id              | bigint       | primary key, auto_increment  |
// | first_name      | varchar(32)  | not null                     |
// | last_name       | varchar(32)  | not null                     |
// | user_entity_id  | bigint       | not null                     |
// +─────────────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class UserProfileEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 32,nullable = false)
    private String firstName;
    @Column(length = 32, nullable = false)
    private String lastName;
    @OneToOne
    @JoinColumn
    private  UserEntity userEntity;

    public Long getId() {
        return id;
    }

    public UserProfileEntity(String firstName) {
        this.firstName = firstName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public UserProfileEntity(String firstName, String lastName, UserEntity userEntity) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userEntity = userEntity;
    }

    public UserProfileEntity(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserProfileEntity() {
    }
}
